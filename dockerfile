# Step 1: Image in a docker file specifies what sorts of application you are developing
FROM python:3.12-slim

# Step 2: Working directory is where the project file will exist
WORKDIR /

# Step 3: Run initialization commands (install any package or software needed)
RUN apt-get update

# Step 4: Create a virtual environemnt
RUN python -m venv /opt/venv
# python -m venv <location_of_venv>

# Step 5: Set the ENV path to Python venv
ENV PATH="/opt/venv/bin:$PATH"

# Step 6: Copy project files to the image
COPY . .
# "." means all files

# Step 7: Install Python requirements
RUN pip install -r requirements.txt


# Step 8: Run the project inside the container
EXPOSE 5000
CMD ["python", "-u", "main.py"]