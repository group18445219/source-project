from flask import Flask
from dotenv import load_dotenv

# import dns.resolver
# dns.resolver.default_resolver = dns.resolver.Resolver(configure=False)
# dns.resolver.default_resolver.nameservers = ['8.8.8.8']

app = Flask(__name__)
load_dotenv("./.env")

# Syntax: os.getenv(<ENVIRONMENT_VARIABLE_NAME>)
#print(os.getenv("CLUSTER_URL"))
#print(os.getenv("MONGODB_USERNAME"))

from app import routes